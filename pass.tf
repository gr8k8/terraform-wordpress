resource "random_password" "db_password" {
  length  = 16
  special = false
}

resource "random_password" "pass_wp" {
  count   = 8
  length  = 64
  special = false
}
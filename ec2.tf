resource "aws_instance" "bastion" {
  ami                         = "ami-0abb03f8142ebd755"
  instance_type               = "t3.nano"
  subnet_id                   = aws_subnet.public-a.id
  associate_public_ip_address = true
  key_name                    = aws_key_pair.masterkey.key_name

  vpc_security_group_ids = [aws_security_group.ssh.id, aws_security_group.ping.id, aws_security_group.allow-out-all.id]

  tags = {
    Name = "bastion"
  }

}

resource "aws_instance" "bakend" {
  count                       = 0
  ami                         = "ami-0abb03f8142ebd755"
  instance_type               = "t3.micro"
  subnet_id                   = aws_subnet.private-a.id
  associate_public_ip_address = false
  key_name                    = aws_key_pair.masterkey.key_name
  user_data                   = templatefile("templates/init.tftpl", { nfs_dns = aws_efs_file_system.wp.dns_name, db_host = aws_db_instance.wp.address, db_name = var.db_name, db_user = var.db_user, db_pass = random_password.db_password.result, auth_key = random_password.pass_wp[0].result, secure_key = random_password.pass_wp[1].result, logged_key = random_password.pass_wp[2].result, nonce_key = random_password.pass_wp[3].result, auth_salt = random_password.pass_wp[4].result, secure_salt = random_password.pass_wp[5].result, logged_salt = random_password.pass_wp[6].result, nonce_salt = random_password.pass_wp[7].result })

  vpc_security_group_ids = [aws_security_group.ssh.id, aws_security_group.ping.id, aws_security_group.allow-out-all.id, aws_security_group.alb_sg.id]

  tags = {
    Name = "bakend"
  }

}

#leaving as it is a public key
resource "aws_key_pair" "masterkey" {
  key_name   = "masterkey"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC8EAnUe5YrUB6qtQizlra6zEnyKyOxTA//2zFqvA3oGAOp23L4nIVdJcJ252LjAvI3CkRE2pTWq6ulSIOasa8hcJLelhjE90bzOr7JszbJ6PA/w5Iu1qKuthaC2rjfKnaxQs9ggJNxMemr9gzjgRsPugIPrqQLbhdZ16R63uQUI+EDl40e55udsPWTaRteMyYdtgHSFC++jj63o3urRHqJl4yPGHjlSviF6Mzy/2UshWJxzV5MSTznN7pNekXg9jevI4uOTw2u4LcpQj/jcVkOS4Za493zIdA/NHd6PMo/WkOEdLg4BGXxa7W1qdMLZ4p5YbovnvYjFa2r3IiJFO2yz81x6k9Dwly9KRhGSZ1KndlSGmWwzD2al0BEw1NL9goaU5ip2PyIdBcKJpeseBySIrjRhkqP7pt2rkxiDzRekpvtLqbs6euOdGH+0iHv6ykJBFl7AiyyRrfl1WyndZ82QN6I8XH7vCs9+kGNN9Xojvr10tUkogizZiOzIFGwhvE= k8"
}


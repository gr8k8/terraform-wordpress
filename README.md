## Name
Terraform-wordpress

## Description
This project's objective is to deploy Wordpress to AWS using RDS and autoscaling group. All resources are defined using Terraform AWS provider. 

## Main components
- VPC - two private and two public subnets
- ALB - application load balancer
- Autoscaling Group with Autoscaling Policy
- CW - cloud watch alarms triggering scale up action when cpu load is over 70% and scale down action when cpu load is under 30%
- EFS - persistent file storage
- RDS - RDS instance with mysql engine

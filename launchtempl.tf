resource "aws_launch_template" "wp" {
  name                   = "wp-template"
  instance_type          = "t3.medium"
  image_id               = "ami-0abb03f8142ebd755"
  key_name               = aws_key_pair.masterkey.key_name
  user_data              = base64encode(templatefile("templates/init.tftpl", { nfs_dns = aws_efs_file_system.wp.dns_name, db_host = aws_db_instance.wp.address, db_name = var.db_name, db_user = var.db_user, db_pass = random_password.db_password.result, auth_key = random_password.pass_wp[0].result, secure_key = random_password.pass_wp[1].result, logged_key = random_password.pass_wp[2].result, nonce_key = random_password.pass_wp[3].result, auth_salt = random_password.pass_wp[4].result, secure_salt = random_password.pass_wp[5].result, logged_salt = random_password.pass_wp[6].result, nonce_salt = random_password.pass_wp[7].result }))
  vpc_security_group_ids = [aws_security_group.ssh.id, aws_security_group.ping.id, aws_security_group.allow-out-all.id, aws_security_group.alb_sg.id]

  monitoring {
    enabled = true
  }

  tags = {
    Name = "bakend"
  }
}
resource "aws_db_instance" "wp" {
  allocated_storage    = 6
  db_name              = var.db_name
  engine               = "mysql"
  engine_version       = "8.0.35"
  instance_class       = "db.t3.micro"
  username             = var.db_user
  password             = random_password.db_password.result
  skip_final_snapshot  = true
  db_subnet_group_name = aws_db_subnet_group.rds.name

  vpc_security_group_ids = [aws_security_group.mysql.id]
}

resource "aws_db_subnet_group" "rds" {
  name       = "rds"
  subnet_ids = [aws_subnet.private-a.id, aws_subnet.private-b.id]

  tags = {
    Name = "rds"
  }
}
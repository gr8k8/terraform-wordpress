resource "aws_cloudwatch_metric_alarm" "cpu_low" {
  alarm_name                = "low_cpu_load"
  comparison_operator       = "LessThanThreshold"
  evaluation_periods        = 2
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = 120
  statistic                 = "Average"
  threshold                 = 30
  alarm_description         = "This metric triggers an alarm when cpu load is under 30%"
  insufficient_data_actions = []
  alarm_actions             = [aws_autoscaling_policy.scale_down.arn]
}

resource "aws_cloudwatch_metric_alarm" "cpu_high" {
  alarm_name                = "high_cpu_load"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = 2
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = 120
  statistic                 = "Average"
  threshold                 = 70
  alarm_description         = "This metric triggers an alarm when cpu load is over 70%"
  insufficient_data_actions = []
  alarm_actions             = [aws_autoscaling_policy.scale_up.arn]
}
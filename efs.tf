resource "aws_efs_file_system" "wp" {
  creation_token = "wp"

  tags = {
    Name = "wp"
  }
}

resource "aws_efs_mount_target" "wp-mount-a" {
  file_system_id  = aws_efs_file_system.wp.id
  subnet_id       = aws_subnet.private-a.id
  security_groups = [aws_security_group.nfs.id]
}

resource "aws_efs_mount_target" "wp-mount-b" {
  file_system_id  = aws_efs_file_system.wp.id
  subnet_id       = aws_subnet.private-b.id
  security_groups = [aws_security_group.nfs.id]
}
resource "aws_security_group" "ssh" {

  name   = "ssh"
  vpc_id = aws_vpc.vpc-tf.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["188.122.3.147/32", "10.73.0.0/16"]
  }
}

resource "aws_security_group" "ping" {

  name   = "ping"
  vpc_id = aws_vpc.vpc-tf.id

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
  }
}

resource "aws_security_group" "allow-out-all" {

  name   = "allow-out"
  vpc_id = aws_vpc.vpc-tf.id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "mysql" {

  name   = "mysql"
  vpc_id = aws_vpc.vpc-tf.id

  ingress {
    from_port        = 3306
    to_port          = 3306
    protocol         = "TCP"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "alb_sg" {
  vpc_id = aws_vpc.vpc-tf.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb_sg"
  }
}

resource "aws_security_group" "nfs" {

  name   = "nfs"
  vpc_id = aws_vpc.vpc-tf.id

  ingress {
    from_port   = 2049
    to_port     = 2049
    protocol    = "TCP"
    cidr_blocks = [aws_subnet.private-a.cidr_block, aws_subnet.private-b.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}


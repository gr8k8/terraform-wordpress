resource "aws_vpc" "vpc-tf" {
  cidr_block           = "10.73.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "vpc-tf"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc-tf.id

  tags = {
    Name = "gw"
  }
}

resource "aws_subnet" "public-a" {
  vpc_id            = aws_vpc.vpc-tf.id
  cidr_block        = "10.73.1.0/24"
  availability_zone = "eu-north-1a"

  tags = {
    Name = "public-a"
  }
}

resource "aws_subnet" "public-b" {
  vpc_id            = aws_vpc.vpc-tf.id
  cidr_block        = "10.73.2.0/24"
  availability_zone = "eu-north-1b"

  tags = {
    Name = "public-b"
  }
}

resource "aws_subnet" "private-a" {
  vpc_id            = aws_vpc.vpc-tf.id
  cidr_block        = "10.73.11.0/24"
  availability_zone = "eu-north-1a"

  tags = {
    Name = "private-a"
  }
}

resource "aws_subnet" "private-b" {
  vpc_id            = aws_vpc.vpc-tf.id
  cidr_block        = "10.73.12.0/24"
  availability_zone = "eu-north-1b"

  tags = {
    Name = "private-b"
  }
}

resource "aws_route_table" "route-public" {
  vpc_id = aws_vpc.vpc-tf.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "route-public"
  }
}

resource "aws_route_table_association" "rt-a" {
  subnet_id      = aws_subnet.public-a.id
  route_table_id = aws_route_table.route-public.id
}

resource "aws_route_table_association" "rt-b" {
  subnet_id      = aws_subnet.public-b.id
  route_table_id = aws_route_table.route-public.id
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.nat-gw.id
  subnet_id     = aws_subnet.public-a.id

  tags = {
    Name = "nat-gw"
  }
}

resource "aws_eip" "nat-gw" {
  domain = "vpc"
}

resource "aws_route_table" "route-priv" {
  vpc_id = aws_vpc.vpc-tf.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat-gw.id
  }

  tags = {
    Name = "route-priv"
  }
}

resource "aws_route_table_association" "rt-priv-a" {
  subnet_id      = aws_subnet.private-a.id
  route_table_id = aws_route_table.route-priv.id
}

resource "aws_route_table_association" "rt-priv-b" {
  subnet_id      = aws_subnet.private-b.id
  route_table_id = aws_route_table.route-priv.id
}
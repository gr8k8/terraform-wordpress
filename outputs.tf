output "bastion_ip" {
  value = aws_instance.bastion.public_ip
}

output "db_addr" {
  value = aws_db_instance.wp.address
}

output "alb_dns_name" {
  value = aws_lb.alb.dns_name
}

output "www-address" {
  value = "www.${data.aws_route53_zone.zone.name}"
}

output "nfs-address" {
  value = aws_efs_file_system.wp.dns_name
}


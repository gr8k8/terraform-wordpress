resource "aws_autoscaling_group" "wp-asg" {
  name                      = "wp-asg"
  max_size                  = 5
  min_size                  = 1
  health_check_grace_period = 300
  health_check_type         = "EC2"
  desired_capacity          = 1
  vpc_zone_identifier       = [aws_subnet.private-a.id, aws_subnet.private-b.id]

  launch_template {
    id      = aws_launch_template.wp.id
    version = "$Latest"
  }

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
    "GroupInServiceCapacity",
    "GroupPendingCapacity",
    "GroupStandbyCapacity",
    "GroupTerminatingCapacity",
    "GroupTotalCapacity"
  ]
}

resource "aws_autoscaling_attachment" "wp-asg" {
  autoscaling_group_name = aws_autoscaling_group.wp-asg.id
  lb_target_group_arn    = aws_lb_target_group.alb_tg.arn
}